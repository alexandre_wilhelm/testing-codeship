package utilities

import (
	"os"
)

// EnvVars is a basic structure with common enviroment variables parsed by the command.
// Target or service specific environment variables will be maintained
// in a per-service or per-infrastructure structure in the corresponding
// packages.
type EnvVars struct {
	envIdentifier string
	envType       string
	envTarget     string
	path          string
}

// ParseEnvVars will parse the options given in parameter and put them in variable of go.
func (e *EnvVars) ParseEnvVars(args map[string]interface{}) {

	var ok bool
	e.envType, ok = args["--env-type"].(string)

	if !ok {
		e.envType = "dev"
	}

	e.envTarget, ok = args["--env-target"].(string)

	if !ok {
		e.envTarget = "kubernetes"
	}

	e.path, ok = args["--path"].(string)

	if !ok {
		e.path = os.Getenv("APORETO_HOME")
	}

	e.envIdentifier, ok = args["--env-identifier"].(string)

	if !ok {
		e.envIdentifier = "aporeto"
	}

	return
}

// NewEnvVars will return a new object of type EnvVars
func NewEnvVars() (env *EnvVars) {
	return &EnvVars{}
}

// EnvTarget will return the envTarget of the env
func (e *EnvVars) EnvTarget() (EnvTarget string) {
	return e.envTarget
}

// EnvType will return the envType of the env
func (e *EnvVars) EnvType() (EnvTarget string) {
	return e.envType
}

// Path will return the path of the env
func (e *EnvVars) Path() (EnvTarget string) {
	return e.path
}

// EnvIdentifier will return the identifier of the env
func (e *EnvVars) EnvIdentifier() (EnvTarget string) {
	return e.envIdentifier
}
