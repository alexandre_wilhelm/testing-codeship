package utilities

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
)

// Basic utility to validate whether an executable path is actually correct.
func pathExists(name string) bool {

	_, err := os.Stat(name)

	if os.IsNotExist(err) {
		return false
	}

	return true
}

// RunScript will run the given commandName and print the results in real time
func RunScript(commandName string, args []string) (err error) {

	if pathExists(commandName) {

		fmt.Println("Executing command:", commandName, args)

		cmd := exec.Command(commandName, args...)
		cmdOutReader, err := cmd.StdoutPipe()

		if err != nil {
			fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
			os.Exit(1)
		}

		scannerOut := bufio.NewScanner(cmdOutReader)

		go func() {
			for scannerOut.Scan() {
				fmt.Printf("%s\n", scannerOut.Text())
			}
		}()

		cmdErrReader, err := cmd.StderrPipe()
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error creating StderrPipe for Cmd", err)
			os.Exit(1)
		}

		scannerErr := bufio.NewScanner(cmdErrReader)

		go func() {
			for scannerErr.Scan() {
				fmt.Printf("%s\n", scannerErr.Text())
			}
		}()

		err = cmd.Start()

		if err != nil {
			fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
			os.Exit(1)
		}

		err = cmd.Wait()

		if err != nil {
			fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
			os.Exit(1)
		}

		return nil
	}

	return fmt.Errorf("Executable not found: ", commandName)
}
