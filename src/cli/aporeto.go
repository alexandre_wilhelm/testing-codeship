package main

import (
	"fmt"
	"github.com/docopt/docopt-go"
	"kubernetes"
)

// Env interface for all infrastructure related methods. Different infrastructure
// environments must be implemented using this interface. The implementation
// can create dev or production environments using Kubernetes, Mesos, AWS, GCP
// or other environments.
type Env interface {
	Start() error
	Stop() error
	Build() error
	Status() error
}

// ervice interface for all infrastructure related methods. Different infrastructure
// environments must be implemented using this interface. The implementation
// can create dev or production environments using Kubernetes, Mesos, AWS, GCP
// or other environments.
type Service interface {
	Start() error
	Stop() error
	Build() error
	Status() error
}

func main() {
	usage := `Aporeto CLI.

Usage: aporeto <command> (build|start|stop|status) [options]
       aporeto -h | --help
       aporeto --version

Options:
       -h, --help                              Show this screen.
       --docker-registry=<docker-registry>     Docker registry to use [default localhost:5000]
       --env-identifier=<env-identifier>       Identifier of the environment
       --env-target=<env-target>               Target environment can be Kubernetes, AWS, Mesos (default Kubernetes)
       --env-type=<env-type>                   Environment type is dev or prod (default dev)
       --path=PATH                             Root path of Aporeto sources [default ./]
       --version                               Show version.

Available commands are:
    env         Manage the environment and infrastructure deployments
    service     Manage the services`

	arguments, _ := docopt.Parse(usage, nil, true, "Aporeto 0.0.1", false)

	cmd := arguments["<command>"].(string)

	err := runCommand(cmd, arguments)

	if err != nil {
		fmt.Println("Failed to execute command\nReason: ", err)
	}
}

func runCommand(cmd string, args map[string]interface{}) (err error) {

	switch cmd {

	case "env":
		return manageEnv(args)

	case "service":
		return manageService(args)

	default:
		return fmt.Errorf("Command not recognized.")
	}

	return
}

// Manage the environment and associate a different environment
// with the implementation depending on the environment target.
func manageEnv(args map[string]interface{}) (err error) {

	// Populate default values for environment
	envTarget, ok := args["--env-target"].(string)

	if !ok {
		envTarget = "kubernetes"
	}

	switch envTarget {

	case "kubernetes":
		return executeEnvCommand(kubernetes.NewInfrastructure(args), args)

	default:
		return fmt.Errorf("Uknown environment type:", envTarget)
	}
}

func executeEnvCommand(env Env, args map[string]interface{}) (err error) {

	if args["start"].(bool) {
		return env.Start()
	}

	if args["stop"].(bool) {
		return env.Stop()
	}

	if args["build"].(bool) {
		return env.Build()
	}

	if args["status"].(bool) {
		return env.Status()
	}

	return fmt.Errorf("Uknown command - Must be one of build/start/stop/status")
}

// Manage services by associating the right implementation based on
// the target environment.
func manageService(args map[string]interface{}) (err error) {

	// Populate default values for environment
	envTarget, ok := args["--env-target"].(string)

	if !ok {
		envTarget = "kubernetes"
	}

	switch envTarget {

	case "kubernetes":
		return executeServiceCommand(kubernetes.NewService(args), args)

	default:
		return fmt.Errorf("Service cannot be started in this environment:", envTarget)
	}

	return
}

func executeServiceCommand(service Service, args map[string]interface{}) (err error) {

	if args["start"].(bool) {
		return service.Start()
	}

	if args["stop"].(bool) {
		return service.Stop()
	}

	if args["build"].(bool) {
		return service.Build()
	}

	if args["status"].(bool) {
		return service.Status()
	}

	return fmt.Errorf("Uknown command - Must be one of build/start/stop/status")
}
