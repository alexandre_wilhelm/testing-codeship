package kubernetes

import (
	"fmt"
	"os/exec"
	"utilities"
)

// Currently implemented with the usual scripts
// We will be replacing this with direct API calls

// Infrastructure is a structure that contains all parameters associate with the instantiation
// of a kubernetes infrastructure. This will not be used in production
// and its mainly for development enviromnets. Production enviroments
// are assumed to have a well maintained infrastructure.
type Infrastructure struct {
	env utilities.EnvVars
}

var (
	buildPath = "/infra/environments/kubernetes/scripts/kubernetes_build.sh"
	startPath = "/infra/environments/kubernetes/scripts/kubernetes_start.sh"
	stopPath  = "/infra/environments/kubernetes/scripts/kubernetes_stop.sh"
)

// Build the kubernetesnfrastructure packages. Mainly pull the corresponding
// Docker containers.
func (c *Infrastructure) Build() (err error) {

	cmdString := c.env.Path() + buildPath
	cmdArgs := []string{c.env.EnvIdentifier()}

	return utilities.RunScript(cmdString, cmdArgs)
}

// Start the kubernetes infrastructure.
func (c *Infrastructure) Start() (err error) {

	cmdString := c.env.Path() + startPath
	cmdArgs := []string{c.env.EnvIdentifier()}

	return utilities.RunScript(cmdString, cmdArgs)
}

// Stop the kubernetes intrastructure.
func (c *Infrastructure) Stop() (err error) {

	cmdString := c.env.Path() + stopPath
	cmdArgs := []string{c.env.EnvIdentifier()}

	return utilities.RunScript(cmdString, cmdArgs)
}

// Status will print a basic status of the infrastructure health.
func (c *Infrastructure) Status() (err error) {

	var cmdString string
	cmdString, err = exec.LookPath("kubectl")

	if err != nil {
		return fmt.Errorf("kubectl cannot be found in the path")
	}

	cmdArgs := []string{"get", "service,pod,rc"}

	return utilities.RunScript(cmdString, cmdArgs)
}

// NewInfrastructure will create a new infrastructure environment. Arguments are passed
// as a map of strings as this was created by dockopt.
func NewInfrastructure(args map[string]interface{}) *Infrastructure {

	var infrastructure Infrastructure
	infrastructure.env.ParseEnvVars(args)

	return &infrastructure
}
