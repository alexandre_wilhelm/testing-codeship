package kubernetes

import (
	"fmt"
	"os/exec"
	"utilities"
)

// Currently implemented with the usual scripts
// We will be replacing this with direct API calls

// Service is a structure that maintains all the parameters associated with starting
// the Aporeto service in a kubernetes environment. It implements
// the interface aporetoService
type Service struct {
	env utilities.EnvVars
}

// Define the paths with the corresponding scripts. This will
// be modified when we transition to a direct API instantiation.
var (
	serviceBuildPath = "/infra/scripts/build_docker_images.sh"
	serviceStartPath = "/infra/environments/kubernetes/scripts/services_start.sh"
	serviceStopPath  = "/infra/environments/kubernetes/scripts/services_stop.sh"
)

// Build the aporeto services for Kubernetes.
func (c Service) Build() (err error) {

	cmdString := c.env.Path() + serviceBuildPath
	cmdArgs := []string{c.env.EnvIdentifier()}

	return utilities.RunScript(cmdString, cmdArgs)
}

// Start the Aporeto services in Kubernetes.
func (c Service) Start() (err error) {

	cmdString := c.env.Path() + serviceStartPath
	cmdArgs := []string{c.env.Path(), c.env.EnvIdentifier()}

	return utilities.RunScript(cmdString, cmdArgs)
}

// Stop the Aporeto services in a Kubernetes environment.
func (c Service) Stop() (err error) {

	cmdString := c.env.Path() + serviceStopPath
	cmdArgs := []string{c.env.Path(), c.env.EnvIdentifier()}

	return utilities.RunScript(cmdString, cmdArgs)
}

// Status will provide a status of the Aporeto services
func (c Service) Status() (err error) {

	var cmdString string
	cmdString, err = exec.LookPath("kubectl")

	if err != nil {
		return fmt.Errorf("kubectl cannot be found in the path")
	}

	cmdArgs := []string{"get", "service,pod,rc"}

	return utilities.RunScript(cmdString, cmdArgs)
}

// NewService will create a new service and parse associated environment variables.
func NewService(args map[string]interface{}) *Service {

	var service Service
	service.env.ParseEnvVars(args)

	return &service
}
